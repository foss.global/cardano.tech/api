# @cardanotech/api
an unofficiall wallet API for the cardano node

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@cardanotech/api)
* [gitlab.com (source)](https://gitlab.com/cardanotech/api)
* [github.com (source mirror)](https://github.com/cardanotech/api)
* [docs (typedoc)](https://cardanotech.gitlab.io/api/)

## Status for master

Status Category | Status Badge
-- | --
GitLab Pipelines | [![pipeline status](https://gitlab.com/cardanotech/api/badges/master/pipeline.svg)](https://lossless.cloud)
GitLab Pipline Test Coverage | [![coverage report](https://gitlab.com/cardanotech/api/badges/master/coverage.svg)](https://lossless.cloud)
npm | [![npm downloads per month](https://badgen.net/npm/dy/@cardanotech/api)](https://lossless.cloud)
Snyk | [![Known Vulnerabilities](https://badgen.net/snyk/cardanotech/api)](https://lossless.cloud)
TypeScript Support | [![TypeScript](https://badgen.net/badge/TypeScript/>=%203.x/blue?icon=typescript)](https://lossless.cloud)
node Support | [![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
Code Style | [![Code Style](https://badgen.net/badge/style/prettier/purple)](https://lossless.cloud)
PackagePhobia (total standalone install weight) | [![PackagePhobia](https://badgen.net/packagephobia/install/@cardanotech/api)](https://lossless.cloud)
PackagePhobia (package size on registry) | [![PackagePhobia](https://badgen.net/packagephobia/publish/@cardanotech/api)](https://lossless.cloud)
BundlePhobia (total size when bundled) | [![BundlePhobia](https://badgen.net/bundlephobia/minzip/@cardanotech/api)](https://lossless.cloud)
Platform support | [![Supports Windows 10](https://badgen.net/badge/supports%20Windows%2010/yes/green?icon=windows)](https://lossless.cloud) [![Supports Mac OS X](https://badgen.net/badge/supports%20Mac%20OS%20X/yes/green?icon=apple)](https://lossless.cloud)

## Usage

Use TypeScript for best in class intellisense

### Prerequisites

- docker needs to be installed on the system
- docker-compose needs to be installed on the system
- the user running the package must be able to talk to docker daemon.

```typescript
import * as cardano from '@cardanotech/api';

const run = async () => {
  await cardano.CardanoApi.startCardanoNodeAndWalletWithDockerCompose(); // starts up cardano node and wallet on the system using docker-compose.
  const cardanoApiInstance = await cardano.CardanoApi.createForDockerCompose(); // static function that creates an instance of CardanoApi
  await cardanoApiInstance.blockchainSyncedPromise; // a promise that resolves once the running cardano-node has synced with the blockchain
  const cardanoWallet = await cardanoApiInstance.restoreWalletFromSeed(['my', 'awesome', 'seed']); // creates a wallet from seed. Alternatively takes a space separated string.
  const expectedPayment = await cardanoWallet.createExpectedPayment(
    100.546,
    'some optional address to forward to upon completion'
  ); // creates an expectedPayment of 100.546, takes a address to forward payments to upon completion

  console.log(expectedPayment.amount); // the amount of the expected payment
  console.log(expectedPayment.targetAddress); // the target address at which the payment should arrive
  console.log(expectedPayment.completedPaymentForwardAddress); // the address that the complete amount will be forwarded to once the expected payment completes successfully
  console.log(expectedPayment.status); // the current status of the payment
  console.log(expectedPayment.eventLog); // an array of events attributed to the expected payment

  const receipt = await expectedPayment.paymentReceivedPromise; // a promise that resolves with a receipt once payment is received.

  // trigger some action here, e.g. delivery of an ebook by email or posting items for shipment in the physical world.
};

run();
```

## Commercial support

Lossless GmbH provides commercial support and consulting for this package.
Please inquire about it by writing us at [hello@lossless.com](mailto://hello@lossless.com)

## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
