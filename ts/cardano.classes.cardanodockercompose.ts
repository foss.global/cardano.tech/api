import * as plugins from './cardano.plugins';
import * as paths from './cardano.paths';

/**
 * a handler for bootstrapping everything with docker compose
 */
class CardanoDockerCompose {
  /**
   *
   */
  public async start() {
    console.log('first pulling images');
    const resultPull = await plugins.dockerCompose.pullAll({
      cwd: paths.assetsDir,
    });

    console.log(resultPull.err);

    console.log('now starting up images');

    const resultUp = await plugins.dockerCompose.upAll({
      cwd: paths.assetsDir,
    });
    console.log(resultUp.err);
  }

  /**
   *
   */
  public async stop() {
    const resultDown = await plugins.dockerCompose.down({
      cwd: paths.assetsDir,
    });
    console.log(resultDown.err);
  }
}

export const defaultCardanoDockerCompose = new CardanoDockerCompose();
